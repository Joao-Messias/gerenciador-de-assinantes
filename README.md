# Gerenciador de Assinantes

## Integrantes

- [João Pedro Meira Messias]
- [Gustavo Luis Guedes]

## Tecnologias utilizadas

- [Node.js]
- [Express]
- [MongoDB]
- [Mongoose]

## Especificações do trabalho

### 1. Funcionalidade de acordo com as chamadas da API

- [x] Criar um usuário [id,nome,sobrenome,data de nascimento,telefone,endereço,cidade,estado,status(ativo, inativo)]
- [x] Listar todos os usuários
- [x] Listar um único usuário por ID
- [x] Listar usuários por Nome,Sobrenome,Cidade,Estado e Status.
- [x] Atualizar um usuário por ID 
- [x] Deletar um usuário por ID
- [x] Opção de upload de foto do usuário -> usar binary para salvar a foto no banco de dados

### 2. Endpoints atendendo o que é requerido
- [x] GET /users -> Listar todos os usuários
- [x] GET /users/:id -> Listar um único usuário por ID
- [x] GET /users?nome=nome -> Filtrar por nome
- [x] GET /users?sobrenome=sobrenome -> Filtrar por sobrenome
- [x] GET /users?cidade=cidade -> Filtrar por cidade
- [x] GET /users?estado=estado -> Filtrar por estado
- [x] GET /users?status=status -> Filtrar por status
- [x] POST /users -> Criar um usuário
- [x] PUT /users/:id -> Atualizar um usuário por ID
- [x] DELETE /users/:id -> Deletar um usuário por ID
- [x] POST /users/upload/:id -> Upload de foto do usuário
- [x] PUT /users/upload/:id -> Atualizar foto do usuário

### 3. Tratativas de erro e chamadas corretas (uso de http code e tipos de request específicos para as funcionalidades)

## Para rodar o projeto rode os seguintes comandos

```
cd backend-gerenciador-de-assinantes-express-generator
npm install
npm start
```

Este projeto roda na porta 3000.

Basta acessar http://localhost:3000/ para acessar a página inicial.

## Segue Documentação da API:
https://documenter.getpostman.com/view/25012216/2s93sW9Fif#b82f68c2-609d-44bc-be8a-c63822f67562