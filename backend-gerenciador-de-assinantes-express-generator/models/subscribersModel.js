const mongoose = require('mongoose');

const subscriberSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: true,
        unique: true,
    },

    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    dateOfBirth: {
        type: Date,
        required: true
    },
    cellphone: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    status: {
        type: Boolean,
        default: true,
        required: true
    },

    profileImage: {
        type: String,
        required: true
    }

} , { timestamps: true });

module.exports = mongoose.model('subscriber', subscriberSchema);