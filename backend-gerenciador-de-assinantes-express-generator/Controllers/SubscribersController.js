const subscriberModel = require("../models/subscribersModel");

class SubscribersController {
  async listAll(req, res) {
    const resultado = await subscriberModel.find({});

    if (resultado.length == 0) {
      return res.status(400).json("Nenhum assinante encontrado!");
    }

    res.status(200).json(resultado);
    return resultado;
  }

  async getInfoById(req, res) {
    try {
      const id = Number(req.params.id);

      if (!id) {
        return res.status(400).json("Erro ao encontrar ID!");
      }

      const resultado = await subscriberModel.findOne({ id: id });

      if (!resultado) {
        return res.status(400).json("Erro ao encontrar usuário por ID");
      }

      res.status(200).json(resultado);
      return resultado;
    } catch (error) {
      console.log(error.message);
    }
  }

  async getUsersByStatus(req, res) {
    try {

      const Status = req.params.status;

      if (!Boolean(Status)) {
        return res.status(400).json("Parameter must be true or false");
      };

      const users = await subscriberModel.find({ 'status': Status });

      if (users.length == 0) {
        return res.status(400).json("Nenhum usuário com este status.");
      }

      return res.status(200).json(users);
    } catch (error) {
      return res
        .status(400)
        .json({ error: "Ocorreu um erro ao buscar as informações pelo nome." });
    }
  }

  async getInfoByFirstName(req, res) {
    try {

      const name = req.params.firstName;

      const users = await subscriberModel.find({ 'firstName': name });

      if (users.length == 0) {
        return res.status(400).json("Nenhum usuário com esse sobrenome.");
      }

      return res.status(200).json(users);
    } catch (error) {
      console.log(error.message)
      res
        .status(400)
        .json({ error: "Ocorreu um erro ao buscar as informações pelo nome." });
    }
  }

  async getInfoByLastName(req, res) {
    try {
        const name = req.params.lastName;

        const users = await subscriberModel.find({ 'lastName': name });

        if (users.length == 0) {
          return res.status(400).json("Nenhum usuário com esse nome.");
        }

        return res.status(200).json(users);

    } catch (error) {
      res
        .status(400)
        .json({ error: "Ocorreu um erro ao buscar as informações pelo nome." });
    }
  }

  async getInfoByCity(req, res) {
    try {

      const city = req.params.city;
      const users = await subscriberModel.find({ 'city': city });

      if (users.length == 0) {
        return res.status(400).json("Nenhum usuário com esse nome.");
      }

      return res.status(200).json(users);

    } catch (error) {
      return res.status(400).json({
        error: "Ocorreu um erro ao buscar as informações pela cidade.",
      });
    }
  }

  async getInfoByState(req, res) {
    try {
      const state = req.params.state;

      const users = await subscriberModel.find({ 'state': state });

      if (users.length == 0) {
        return res.status(400).json("Nenhum usuário com esse nome.");
      }

      return res.status(200).json(users);
    } catch (error) {
      return res.status(400).json({
        error: "Ocorreu um erro ao buscar as informações pelo estado.",
      });
    }
  }

  async setSubscriber(req, res) {
    try {
      let sub = req.body;
      const max = await subscriberModel.findOne({}).sort({ id: -1 });

      if (max == null) {
        sub.id = 1;
      } else {
        sub.id = max.id;
      }

      sub.id = max == null ? 1 : sub.id + 1;
      const resultado = await subscriberModel.create(sub);

      return res
        .status(201)
        .json({ message: "Assinante cadastrado com sucesso.", resultado });
    } catch (error) {
      console.log(error.message);
      return res
        .status(400)
        .json({ error: "Ocorreu um erro ao criar um novo assinante." });
    }
  }

  async updateSubscriber(req, res) {
    try {
      const id = req.params.id;

      const user = await subscriberModel.findOne({ id: id });

      if (!user) {
        return res.status(400).json("Usuário não existe.");
      }

      const updatedData = req.body;

      if (updatedData.length == 0) {
        return res.status(400).json("Você não pode enviar um json vazio.");
      }

      const resultado = await subscriberModel.updateOne(
        { id: id },
        updatedData
      );

      if (resultado.modifiedCount === 0) {
        return res.status(400).json("Erro ao atualizar o usuário");
      } else {
        return res.status(200).json("Atualizado com Sucesso!");
      }
    } catch (error) {
      return res
        .status(400)
        .json({ error: "Ocorreu um erro ao atualizar o assinante." });
    }
  }

  async deleteSubscriber(req, res) {
    try {
      const id = req.params.id;

      const user = await subscriberModel.findOne({ id: id });

      if (!user) {
        return res.status(400).json("Usuário não existe.");
      }

      const resultado = await subscriberModel.deleteOne({ id: id });

      if (resultado.deletedCount === 0) {
        return res.status(400).json("Erro ao deletar o usuário");
      }

      res
        .status(200)
        .json({ message: "Assinante deletado com sucesso.", resultado });
    } catch (error) {
      res
        .status(400)
        .json({ error: "Ocorreu um erro ao deletar o assinante." });
    }
  }
}

module.exports = new SubscribersController();
