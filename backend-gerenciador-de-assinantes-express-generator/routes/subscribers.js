const express = require('express');
const router = express.Router();
const subscribersController = require('../Controllers/SubscribersController');

/* Obter todos os assinantes */
router.get('/', subscribersController.listAll);

/* Obter informações de um assinante pelo ID */
router.get('/:id', subscribersController.getInfoById);

/* Obter informações de assinantes pelo primeiro nome */
router.get('/search/first-name/:firstName', subscribersController.getInfoByFirstName);

/* Obter informações de assinantes pelo sobrenome */
router.get('/search/last-name/:lastName', subscribersController.getInfoByLastName);

/* Obter informações de assinante por cidade */
router.get('/search/city/:city', subscribersController.getInfoByCity);

/* Obter informações de assinante por estado */
router.get('/search/state/:state', subscribersController.getInfoByState);

/* Obter assinantes ativos */
router.get('/search/status/:status', subscribersController.getUsersByStatus);

/* Registrar um novo assinante */
router.post('/register', subscribersController.setSubscriber);

/* Atualizar informações de um assinante */
router.put('/update/:id', subscribersController.updateSubscriber);

/* Excluir um assinante */
router.delete('/delete/:id', subscribersController.deleteSubscriber);

module.exports = router;
